console.log(document.head);
console.log(document.body);
document.body.childNodes.forEach(el => {
  console.log(el);
});

console.log(document.body.querySelector("div"));
document.body.querySelector("div").childNodes.forEach(el => {
  console.log(el);
});

document.body.querySelector("div").childNodes.forEach(el => {
  if (
    el === document.body.querySelector("div").firstElementChild ||
    el === document.body.querySelector("div").lastElementChild
  ) {
  } else {
    console.log(el);
  }
});

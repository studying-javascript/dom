//@Task_1

let pText = document.querySelector("p").textContent;

//@Task_2

function nodeInform(node) {
  obj = {
    name: node.nodeName,
    children: node.children ? node.children.length : 0
  };
  switch (node.nodeType) {
    case 1:
      obj.type = "element node";
      break;
    case 3:
      obj.type = "text node";
      break;
    case 8:
      obj.type = "comment node";
      break;
  }
  return obj;
}

nodeInform(document.querySelector("mark"));

//@Task_3

function getTextFromUl(ul) {
  let arr = [];
  ul.querySelectorAll("a").forEach(el => {
    arr.push(el.text);
  });
  return arr;
}
getTextFromUl(document.querySelector("ul"));

//@Task_4

document.querySelector("p").childNodes.forEach(el => {
  if (nodeInform(el).type === "text node") {
    el.data = " -text- ";
  }
});



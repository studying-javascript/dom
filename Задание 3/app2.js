// *Task_1
document.querySelector("ul").classList.add("list");

// *Task_2

let a = document.querySelectorAll("a");
console.log(a[a.length - 1]);

a[a.length - 1].setAttribute("id", "link");
console.log(a[a.length - 1].attributes);

// *Task_3

let ul = document.querySelector("ul").children;
console.log(ul);

for (let i = 0; i < ul.length; i++) {
  if (i % 2 === 0) {
    ul[i].classList.add("item");
    console.log(ul[i]);
  }
}

// *Task_4

let aAll = document.querySelectorAll("a");
aAll.forEach(el => {
  el.classList.add("custom-link");
});

// *Task_1

let number = document.querySelector("ul").children.length;
let fragment = document.createDocumentFragment();
for (let i = number + 1; i <= number + 2; i++) {
  let li = document.createElement("li");
  li.textContent = `item ${i}`;
  li.classList.add("new-item");
  fragment.appendChild(li);
}

document.querySelector("ul").appendChild(fragment);

// *Task_2

for (let i = 0; i < document.querySelector("ul").children.length; i++) {
  let a = document.querySelector("ul").children[i].querySelector("a");
  let span = document.createElement("span");
  span.textContent = ` span ${i}`;
  if (a) {
    a.appendChild(span);
  }
}

// *Task_3

let img = document.createElement("img");
img.alt = "some_text";
img.src =
  "https://avatars.mds.yandex.net/get-pdb/33827/b76c6286-45f6-44cb-8162-853f81ae5387/s1200";

document.body.insertAdjacentElement("afterbegin", img);

// *Task_4

let mark = document.querySelector("mark");
mark.classList.add("green");
mark.textContent += " green";

// *Task_5

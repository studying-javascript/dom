// *Task_1
let btnMsg = document.querySelector("#btn-msg");
btnMsg.onclick = function(e) {
  // e.stopPropagation();
  alert(btnMsg.dataset.text);
};

// *Task_2
btnMsg.onmouseover = function() {
  btnMsg.classList.add("btn-danger");
};

btnMsg.onmouseout = function() {
  btnMsg.classList.remove("btn-danger");
};

// *Task_3
document.body.onclick = e => {
  document.querySelector("#tag").textContent = `Tag: ${e.target.tagName}`;
};

// *Task_4
let btnGenerate = document.querySelector("#btn-generate");
btnGenerate.onclick = e => {
  let ul = document.querySelector("ul");
  let li = document.createElement("li");
  li.textContent = `Item ${ul.children.length + 1}`;
  ul.appendChild(li);
};

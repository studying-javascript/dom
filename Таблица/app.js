const users = [
  {
    _id: "5d220b10e8265cc978e2586b",
    isActive: true,
    balance: 2853.33,
    age: 20,
    name: "Buckner Osborne",
    gender: "male",
    company: "EMPIRICA",
    email: "bucknerosborne@empirica.com",
    phone: "+1 (850) 411-2997",
    registered: "2018-08-13T04:28:45 -03:00",
    nestedField: { total: 300 }
  },
  {
    _id: "5d220b10144ef972f6c2b332",
    isActive: true,
    balance: 1464.63,
    age: 38,
    name: "Rosalie Smith",
    gender: "female",
    company: "KATAKANA",
    email: "rosaliesmith@katakana.com",
    phone: "+1 (943) 463-2496",
    registered: "2016-12-09T05:15:34 -02:00",
    nestedField: { total: 400 }
  },
  {
    _id: "5d220b1083a0494655cdecf6",
    isActive: false,
    balance: 2823.39,
    age: 40,
    name: "Estrada Davenport",
    gender: "male",
    company: "EBIDCO",
    email: "estradadavenport@ebidco.com",
    phone: "+1 (890) 461-2088",
    registered: "2016-03-04T03:36:38 -02:00",
    nestedField: { total: 200 }
  }
];

let table = document.createElement("table");
table.classList.add("table");

let headers = ["#", "Name", "Email", "Balance"];
let thead = document.createElement("thead");
headers.forEach(el => {
  th = document.createElement("th");
  th.setAttribute("scope", "row");
  th.textContent = el;
  thead.appendChild(th);
});

function createTable(arr) {
  let totalBalance = 0;
  if (document.querySelector("table")) {
    document
      .querySelector("table")
      .removeChild(document.querySelector("table").querySelector("tbody"));
  }
  let tbody = document.createElement("tbody");
  arr.forEach((el, index) => {
    let tr = document.createElement("tr");
    for (i = 0; i < headers.length; i++) {
      switch (i) {
        case 0:
          th = document.createElement("th");
          th.setAttribute("scope", "row");
          th.textContent = index + 1;
          tr.appendChild(th);
          break;
        case 1:
          td = document.createElement("td");
          td.textContent = el.name;
          tr.appendChild(td);
          break;
        case 2:
          td = document.createElement("td");
          td.textContent = el.email;
          tr.appendChild(td);
          break;
        case 3:
          td = document.createElement("td");
          td.textContent = el.balance;
          tr.appendChild(td);
          break;
      }
    }
    totalBalance += el.balance;
    tbody.appendChild(tr);
  });
  let b = document.createElement("b");
  b.textContent = totalBalance.toFixed(2);
  td = document.createElement("td");
  td.textContent = `Total balance: `;
  td.appendChild(b);
  td.setAttribute("colspan", headers.length);
  td.setAttribute("align", "right");
  tbody.appendChild(td);
  return tbody;
}

let tbody = createTable(users);

table.appendChild(thead);
table.appendChild(tbody);
document.querySelector(".container").appendChild(table);

let btn = document.querySelector("button");
btn.onclick = e => {
  let i = btn.querySelector("i");
  if (i.classList.contains("fa-arrow-down")) {
    i.classList.remove("fa-arrow-down");
    i.classList.add("fa-arrow-up");
    let sortedArr = users.sort((prev, next) => {
      return prev.balance - next.balance;
    });
    tbody = createTable(sortedArr);
    table.appendChild(tbody);
  } else {
    i.classList.remove("fa-arrow-up");
    i.classList.add("fa-arrow-down");
    let sortedArr = users.sort((prev, next) => {
      return next.balance - prev.balance;
    });
    tbody = createTable(sortedArr);
    table.appendChild(tbody);
  }
};

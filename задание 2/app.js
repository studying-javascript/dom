function isParent(parent, child) {
  while (!(child.parentElement === parent) && child !== document.body) {
    child = child.parentElement;
  }
  if (child.parentElement === parent) {
    return true;
  }
  return false;
}

// console.log(
//   isParent(document.body.children[0], document.querySelector("mark"))
// );

// console.log(
//   isParent(document.querySelector("ul"), document.querySelector("mark"))
// );

function aIsNotUl(collection) {
  let newCollection = [];
  collection.forEach(el => {
    if (!isParent(document.querySelector("ul"), el)) {
      newCollection.push(el);
    }
  });
  return newCollection;
}

// console.log(aIsNotUl(document.querySelectorAll("a")));

console.log(
  document.querySelector("ul").previousElementSibling,
  document.querySelector("ul").nextElementSibling
);
